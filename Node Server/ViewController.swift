//
//  ViewController.swift
//  Node Server
//
//  Created by Gary Davis on 3/13/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    var username = UITextField()
    //var usernameInfo
    var password = UITextField()
    var loginButton = UIButton()
    var registerButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let dismiss: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(dismiss)
        
        username = UITextField(frame: CGRect(x: 50, y: 200, width: self.view.frame.size.width - 250, height: 75))
        username.text = "Username"
        username.textAlignment = NSTextAlignment.left
        username.font = UIFont.systemFont(ofSize: 35)
        username.translatesAutoresizingMaskIntoConstraints = false
        username.backgroundColor = UIColor.gray
        self.view.addSubview(username)
        
        password = UITextField(frame: CGRect(x: 80, y: 200, width: self.view.frame.size.width - 250, height: 75))
        password.text = "Password"
        password.textAlignment = NSTextAlignment.left
        password.font = UIFont.systemFont(ofSize: 35)
        password.translatesAutoresizingMaskIntoConstraints = false
        password.backgroundColor = UIColor.gray
        self.view.addSubview(password)
        
        let loginButton = UIButton(type: UIButtonType.system)
        loginButton.frame = CGRect(x: 10, y:400, width: self.view.frame.size.width - 20, height: 50)
        loginButton.setTitle("Login", for: UIControlState.normal)
        loginButton.backgroundColor = UIColor.blue
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(loginButton)
        
        let registerButton = UIButton(type: UIButtonType.system)
        registerButton.frame = CGRect(x: 10, y:400, width: self.view.frame.size.width - 20, height: 50)
        registerButton.setTitle("Register Here", for: UIControlState.normal)
        registerButton.backgroundColor = UIColor.orange
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(registerButton)
        registerButton.addTarget(self, action: #selector(registerButtonTouched(register:)), for: UIControlEvents.touchDown)
        
        let masterView = Dictionary(dictionaryLiteral: ("username", username), ("password", password), ("login", loginButton), ("register", registerButton))
        
        let usernameX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[username(300)]", options: [], metrics: nil, views: masterView)
        let usernameY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[username(50)]", options: [], metrics: nil, views: masterView)
        self.view.addConstraints(usernameX)
        self.view.addConstraints(usernameY)
        
        let passwordX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[password(300)]", options: [], metrics: nil, views: masterView)
        let passwordY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-180-[password(50)]", options: [], metrics: nil, views: masterView)
        self.view.addConstraints(passwordX)
        self.view.addConstraints(passwordY)
        
        let loginButtonX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-160-[login(120)]", options: [], metrics: nil, views: masterView)
        let loginButtonY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-250-[login(50)]", options: [], metrics: nil, views: masterView)
        self.view.addConstraints(loginButtonX)
        self.view.addConstraints(loginButtonY)
        
        let registerButtonX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-160-[register(120)]", options: [], metrics: nil, views: masterView)
        let registerButtonY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-320-[register(50)]", options: [], metrics: nil, views: masterView)
        self.view.addConstraints(registerButtonX)
        self.view.addConstraints(registerButtonY)
    
    }
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func registerButtonTouched(register:UIButton) -> Void {
        performSegue(withIdentifier: "someSegue", sender: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

