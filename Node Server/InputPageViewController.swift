//
//  InputPageViewController.swift
//  Node Server
//
//  Created by Gary Davis on 3/17/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

import UIKit

class InputPageViewController: UIViewController {
    
    var favoriteSchool = UITextField()
    var results = UITextView()
    var logoutButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        favoriteSchool = UITextField(frame: CGRect(x: 50, y: 200, width: self.view.frame.size.width - 250, height: 75))
        favoriteSchool.text = "Enter your favorite school"
        favoriteSchool.textAlignment = NSTextAlignment.left
        favoriteSchool.font = UIFont.systemFont(ofSize: 35)
        favoriteSchool.translatesAutoresizingMaskIntoConstraints = false
        favoriteSchool.backgroundColor = UIColor.gray
        self.view.addSubview(favoriteSchool)
        
        results = UITextView(frame: CGRect(x: 50, y: 200, width: self.view.frame.size.width - 250, height: 75))
        results.text = ""
        results.textAlignment = NSTextAlignment.left
        results.font = UIFont.systemFont(ofSize: 35)
        results.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(results)
        
        let logoutButton = UIButton(type: UIButtonType.system)
        logoutButton.frame = CGRect(x: 10, y:400, width: self.view.frame.size.width - 20, height: 50)
        logoutButton.setTitle("Enter", for: UIControlState.normal)
        logoutButton.backgroundColor = UIColor.red
        logoutButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(logoutButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
