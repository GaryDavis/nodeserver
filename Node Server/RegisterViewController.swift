//
//  RegisterViewController.swift
//  Node Server
//
//  Created by Gary Davis on 3/17/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    var enterUsername = UITextField()
    //var usernameInfo
    var enterPassword = UITextField()
    var enterEmail = UITextField()
    var enterButton = UIButton()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        let dismiss: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(dismiss)
        
        enterUsername = UITextField(frame: CGRect(x: 50, y: 200, width: self.view.frame.size.width - 250, height: 75))
        enterUsername.text = "Enter username"
        enterUsername.textAlignment = NSTextAlignment.left
        enterUsername.font = UIFont.systemFont(ofSize: 35)
        enterUsername.translatesAutoresizingMaskIntoConstraints = false
        enterUsername.backgroundColor = UIColor.gray
        self.view.addSubview(enterUsername)
        
        enterPassword = UITextField(frame: CGRect(x: 80, y: 200, width: self.view.frame.size.width - 250, height: 75))
        enterPassword.text = "Enter password"
        enterPassword.textAlignment = NSTextAlignment.left
        enterPassword.font = UIFont.systemFont(ofSize: 35)
        enterPassword.translatesAutoresizingMaskIntoConstraints = false
        enterPassword.backgroundColor = UIColor.gray
        self.view.addSubview(enterPassword)
        
        enterEmail = UITextField(frame: CGRect(x: 80, y: 200, width: self.view.frame.size.width - 250, height: 75))
        enterEmail.text = "Enter e-mail"
        enterEmail.textAlignment = NSTextAlignment.left
        enterEmail.font = UIFont.systemFont(ofSize: 35)
        enterEmail.translatesAutoresizingMaskIntoConstraints = false
        enterEmail.backgroundColor = UIColor.gray
        self.view.addSubview(enterEmail)
        
        let enterButton = UIButton(type: UIButtonType.system)
        enterButton.frame = CGRect(x: 10, y:400, width: self.view.frame.size.width - 20, height: 50)
        enterButton.setTitle("Enter", for: UIControlState.normal)
        enterButton.backgroundColor = UIColor.blue
        enterButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(enterButton)
        
        let masterView = Dictionary(dictionaryLiteral: ("enterUsername", enterUsername), ("enterPassword", enterPassword), ("enter", enterButton), ("email", enterEmail))
        
        let enterUsernameX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[enterUsername(300)]", options: [], metrics: nil, views: masterView)
        let enterUsernameY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[enterUsername(50)]", options: [], metrics: nil, views: masterView)
        self.view.addConstraints(enterUsernameX)
        self.view.addConstraints(enterUsernameY)
        
        let enterPasswordX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[enterPassword(300)]", options: [], metrics: nil, views: masterView)
        let enterPasswordY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-180-[enterPassword(50)]", options: [], metrics: nil, views: masterView)
        self.view.addConstraints(enterPasswordX)
        self.view.addConstraints(enterPasswordY)
        
        let enterEmailX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[email(300)]", options: [], metrics: nil, views: masterView)
        let enterEmailY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-260-[email(50)]", options: [], metrics: nil, views: masterView)
        self.view.addConstraints(enterEmailX)
        self.view.addConstraints(enterEmailY)
        
        let enterButtonX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-160-[enter(120)]", options: [], metrics: nil, views: masterView)
        let enterButtonY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-300-[enter(50)]", options: [], metrics: nil, views: masterView)
        self.view.addConstraints(enterButtonX)
        self.view.addConstraints(enterButtonY)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
